1. Choose a web-app open source project from Github/Gitlab and fork it (Describe why you choose that particular project)
2. Create a CI pipeline/workflow for the project you forked (Use any tool you like)
3. The CI pipeline/workflow will include anything that is relevant to the project you forked. For example:
    - If it's a Python project, you will run PEP8
    - If the project has unit tests directory, you will run these unit tests as part of the CI
4. In a separate file, describe what is running as part of the CI and why you chose to include it. You can also describe any thoughts, dilemmas, challenge you had
5. Containerize the app of the project you forked using any containerization technology you would like.
6. Write a pipeline that will deploy the containerized app to Kubernetes
7. The CI/CD system (where the pipeline resides) and the Kubernetes cluster should be on separate systems
8. The web app should be accessible remotely and only with HTTPS
9. Document everything you did. In the documentation include:
    - Pipeline code (if you use jenkins include the jenkinsfile, if you use gitlab ci include the yaml, etc.)
    - Doc detailing everything you did and why
    - Dockerfile
    - The code you forked
    
 Put everything into a public or private repo (make sure we can access it) and share it with us!

